import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        openFirst: false,
        openSecond: false,
    }

    render() {
        const {openFirst, openSecond} = this.state;

        const modalHead1 = 'Do you want to delete this file?'
        const modalBody1 = 'Once you delete this file, it won’t be possible to undo this action. ' +
            '\n' + 'Are you sure you want to delete it?'
        const modalHead2 = 'Windows Defender'
        const modalBody2 = 'Your copy of Windows is infected, \n' +
            'Are you sure you want to delete it?'


        return (
            <div className="App">
                <Button onClick={() => {
                    this.openFunc('openFirst')
                }}
                        text={'Open first modal'}
                        className={'button'}
                        backgroundColor={'#000000'}/>
                <Button onClick={() => {
                    this.openFunc('openSecond')
                }}
                        text={'Open second modal'}
                        className={'button'}
                        backgroundColor={'#000000'}/>
                <Modal header={modalHead1}
                       closeButton={true}
                       text={modalBody1}
                       isOpen={openFirst}
                       closeFunc={this.closeFunc}>
                    <Button onClick={this.closeFunc}
                            text={'OK'}
                            className={'button'}
                            backgroundColor={'#00000030'}/>
                    <Button onClick={this.closeFunc}
                            text={'Cancel'}
                            className={'button'}
                            backgroundColor={'#00000030'}/>
                </Modal>
                <Modal header={modalHead2}
                       closeButton={true}
                       text={modalBody2}
                       isOpen={openSecond}
                       closeFunc={this.closeFunc}>
                    <Button onClick={this.closeFunc}
                            text={'Delete'}
                            className={'button'}
                            backgroundColor={'#456056'}/>
                    <Button onClick={this.closeFunc}
                            text={'Delete'}
                            className={'button'}
                            backgroundColor={'#73937E'}/>
                </Modal>
            </div>
        );
    }

    closeFunc = () => {
        this.setState({
            openFirst: false,
            openSecond: false
        });
    }

    openFunc = (e) => {
        this.setState({[e]: true})
    }
}

export default App;
