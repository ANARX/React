import React, {Component} from 'react';
import './Modal.scss'
import Button from "../Button/Button";

class Modal extends Component {
    render() {
        const {header, closeButton, text, children, isOpen, closeFunc} = this.props;

        return (
            <>
                {isOpen && <div className='tint'>
                    <div className='modal'>
                        <div className='header'>
                            <div className='title'>
                                {header}
                            </div>
                            {closeButton && <Button
                                className='close'
                                text={'×'}
                                onClick={closeFunc}/>}
                        </div>
                        <div className='body'>
                            {text}
                        </div>
                        <div className='footer'>
                            {children}
                        </div>
                    </div>
                </div>
                }
            </>
        );
    }
}

export default Modal;
