import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    render() {
        const {text, onClick, backgroundColor, className} = this.props;
        const bgColor = {backgroundColor}

        return (
            <button
                className={className}
                style={bgColor}
                onClick={onClick}>
                {text}
            </button>
        );
    }
}

export default Button;
