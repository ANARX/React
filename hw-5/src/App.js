import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Route} from "react-router-dom";
import Item from "./component/Items/Item";
import {Root} from "./mainStyle";
import Main from './component/Nav/Nav';
import {getData} from './reducers/data/services'
import {cartSet, favoriteSet} from "./reducers/item/services";

const App = (props) => {
    const {data, loadedData, cartSet, favoriteSet, cart, favorite} = props;
    useEffect(() => {
        if (!data.length) {
            loadedData()
        }
        favoriteSet(JSON.parse(localStorage.getItem('favorite')) || []);
        cartSet(JSON.parse(localStorage.getItem('cart')) || [])
    }, []);

    return (
        <>
            {data.length && <Root>
                <Main/>
                <Route exact path='/'>
                    <Item
                        filter={data}/>
                </Route>
                <Route exact path='/cart'>
                    <Item
                        filter={cart} showCart={true}/>
                </Route>
                <Route exact path='/favorite'>
                    <Item
                        filter={favorite}/>
                </Route>
            </Root>}
        </>

    )
}, mapStateToProps = ({loadData, item}) => ({
    data: loadData.data,
    isLoading: loadData.isLoading,
    favorite: item.favorite,
    cart: item.cart,
}), mapDispatchToProps = (dispatch) => ({
    loadedData: () => dispatch(getData()),
    cartSet: (state, id) => dispatch(cartSet(state, id)),
    favoriteSet: (state, id) => dispatch(favoriteSet(state, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

