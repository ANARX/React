const initialState = {
    favorite: [],
    cart: []
}, reducer = (state = initialState, action) => {
    switch (action.type) {
        case "TO_FAVORITE":
            return {...state, favorite: action.payload};
        case "REMOVE_FAVORITE":
            return {...state, favorite: action.payload};
        case "TO_CART":
            return {...state, cart: action.payload};
        case "REMOVE_CART":
            return {...state, cart: action.payload};
        case "SOLD":
            return {...state, cart: []};
        default:
            return state;
    }
};
export default reducer;