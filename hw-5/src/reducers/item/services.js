import {toFavorite, removeFavorite, toCart, removeCart} from './actions';

export const cartSet = (state, card) => (dispatch) => {
    if (!card) {
        dispatch({
            type: 'TO_CART',
            payload: state
        })
    } else {
        const distributor = state.findIndex(elem => elem.id === card.id);
        if (distributor !== -1) {
            dispatch(removeCart(state, distributor));
        } else {
            dispatch(toCart(state, card))
        }
    }
}, favoriteSet = (state, item) => (dispatch) => {
    if (!item) {
        dispatch({
            type: 'TO_FAVORITE',
            payload: state
        })
    } else {
        const distributor = state.findIndex(elem => elem.id === item.id);
        if (distributor !== -1) {
            dispatch(removeFavorite(state, distributor));
        } else {
            dispatch(toFavorite(state, item))
        }
    }
};