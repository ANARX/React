const findAndCut = (array, index, storage) => {
    console.log(array);
    array.splice(index, 1);

    const newArray = array.splice(0);
    if (newArray.length === 0) {
        localStorage.removeItem(storage);
    } else {
        localStorage.setItem(storage, JSON.stringify(newArray));
    }
    return newArray
}, pushToState = (array, elem, storage) => {
    array.push(elem);
    localStorage.setItem(storage, JSON.stringify(array));
    return array
};
export const toFavorite = (array, elem) => ({
    type: 'TO_FAVORITE',
    payload: pushToState(array, elem, 'favorite')
}), removeFavorite = (array, index) => ({
    type: 'REMOVE_FAVORITE',
    payload: findAndCut(array, index, 'favorite')
}), toCart = (array, elem) => ({
    type: 'TO_CART',
    payload: pushToState(array, elem, 'cart')
}), removeCart = (array, index) => ({
    type: 'REMOVE_CART',
    payload: findAndCut(array, index, 'cart')
});