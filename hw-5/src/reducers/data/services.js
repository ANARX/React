import axios from 'axios';
import {loadData, saveData} from './actions';

export const getData = () => (dispatch) => {
    dispatch(loadData(true));
    axios('./db.json')
        .then(res => {
            dispatch(saveData(res.data));
            dispatch(loadData(false));
        })
};