import {combineReducers} from "redux";
import loadData from './data/reducer';
import item from './item/reducer';
import window from './modal/reducer';

export const reducer = combineReducers({
    loadData,
    item,
    window
});