const initialState = {
    open: false
}, reducer = (state = initialState, action) => {
    switch (action.type) {
        case "OPEN_WINDOW":
            return {...state, open: action.payload};
        case "ClOSE_WINDOW":
            return {...state, open: action.payload};
        default:
            return state;
    }
};
export default reducer;