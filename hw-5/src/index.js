import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import App from './App.js';
import * as serviceWorker from './serviceWorker';
import {reducer} from './reducers/reducer';
import {composeWithDevTools} from "redux-devtools-extension";
import './index.css';

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
