import React from 'react';
import {Form, withFormik} from 'formik';
import * as yup from 'yup';
import Input from './Input';
import {ModalBg} from '../../Modal/ModalStyle';
import {Register, Submit} from "./FormStyle";
import {cartSet, favoriteSet} from "../../../reducers/item/services";
import {closeWindow, openWindow} from "../../../reducers/modal/action";
import {connect} from "react-redux";

const REQUIRED = 'Field is required', schema = yup.object().shape({
    name: yup
        .string()
        .required(REQUIRED),
    lastName: yup
        .string()
        .required(REQUIRED),
    age: yup
        .number()
        .positive()
        .required(REQUIRED),
    address: yup
        .string()
        .required(REQUIRED),
    phoneNumber: yup
        .string()
        .matches('^\\+?3?8?(0[5-9][0-9]\\d{7})$')
        .required(REQUIRED)
}), handleSubmit = (values, {props}) => {
    props.inputForm(false);
    localStorage.setItem('cart', JSON.stringify([]));
    props.sold()
}, FormikForm = (props) => {
    const closeWindow = (event) => {
        if (event.currentTarget === event.target) {
            props.inputForm(false)
        }
    };
    return (
        <ModalBg onClick={closeWindow}>
            <Form noValidate>
                <Register>Register</Register>
                <Input
                    type='text'
                    placeholder='First Name'
                    name='name'
                    needTopBorder={true}/>
                <Input
                    type='text'
                    placeholder='Last Name'
                    name='lastName'/>
                <Input
                    type='text'
                    placeholder='Age'
                    name='age'/>
                <Input
                    type='text'
                    placeholder='Address'
                    name='address'/>
                <Input
                    type='text'
                    placeholder='Phone Number'
                    name='phoneNumber'/>
                <div>
                    <Submit
                        type='submit'>Checkout
                    </Submit>
                </div>
            </Form>
        </ModalBg>
    )
}, mapStateToProps = ({item}) => ({
    cart: item.cart,
}), mapDispatchToProps = (dispatch) => ({
    cartSet: (state, id) => dispatch(cartSet(state, id)),
    favoriteSet: (state, id) => dispatch(favoriteSet(state, id)),
    openWindow: (id) => dispatch(openWindow(id)),
    closeWindow: () => dispatch(closeWindow())
}), AddForm = connect(mapStateToProps, mapDispatchToProps)(FormikForm);

export default withFormik({
    mapPropsToValues: (props) => ({
        name: '',
        lastName: '',
        age: '',
        address: '',
        phoneNumber: ''
    }),
    handleSubmit,
    validationSchema: schema
}) (AddForm);