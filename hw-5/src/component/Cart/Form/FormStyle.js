import styled from "styled-components";

const FormInput = styled.input`
        display: block;
        outline: 0;
        background: #31415890;
        border-radius: 5px;
        padding: 10px 15px;
        margin: 0 auto 10px auto;
        text-align: center;
        font-size: 18px;
        transition-duration: 0.7s;
        font-weight: 300;
        &:hover{
        background-color: fade(white, 40%);
        &:focus{
        background-color: #FFFFFF70;
        width: 250px;}
        `;

const Register = styled.h3`
        font-size: 25pt;
        background: #31415850;
        box-shadow: 5px 5px 15px 3px rgba(0, 0, 0, 0.5);
        `;

const Submit = styled.button`
        font-size: 18pt;
        background: #31415850;
        border-radius: 5px;
        margin: 10px;
        padding 5px;
        `;

const Error = styled.div`
        font-size: 10pt;
        color: #FF1C1C80;
        `;

export {
    Register,
    FormInput,
    Submit,
    Error
};