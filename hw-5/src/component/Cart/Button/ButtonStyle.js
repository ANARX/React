import styled from "styled-components";
const BuyButton = styled.button`
        float: right;
        margin: 10px;
        padding:0.5em 1em 0.5em 1em;
        border-radius: 7px;
        border: none;
        font-size: 2em;
        background: #2F3F5699;
        cursor: pointer;
        height: 2em;
        width: 6em;
        transition: all 0.7s ease 0s; 
        box-shadow: 5px 5px 15px 3px rgba(0, 0, 0, 0.5);
        z-index: 1;
        &:hover{
          background-color: #CCE4F790;
        `;

export default BuyButton;