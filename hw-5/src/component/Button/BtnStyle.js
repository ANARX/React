import styled from "styled-components";

const Btn = styled.button`
        border-radius: 3px;
        font-size: 12pt;
        outline: none;
        border: none;
        cursor: pointer;
        color: #FFFFFF;
        text-transform: uppercase;
        transition: all 0.4s ease 0s; 
        margin: 0px 20px 10px 20px;
        width: ${(props) => (props.default ? '100px' : 'auto')};
        padding: ${(props) => (props.default ? '10px 15px' : '5px 10px')};  
        background: ${(props) => (props.buttonBg ? '#B3382C' : '#8E9CAC')};
        &:hover{
        box-shadow: 3px 3px 7px rgba(0, 0, 0, 3);
        color: #bdcdf0;
        }
        `;

export default Btn;