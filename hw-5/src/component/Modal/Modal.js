import React from 'react';
import {ModalBg, ModalWindow, Header, Text} from './ModalStyle.js';
import PropTypes from "prop-types";
import Btn from "../Button/Button";


const Modal = (props) => {
    const outsideClick = (e) => {
        if (e.target === e.currentTarget) {
            props.closeWindow()
        }
    };

    return (
        <ModalBg onClick={(e) => outsideClick(e)}>
            <ModalWindow
                bg={'#E74C3C'}>
                <Header>
                    {props.header}
                </Header>
                <Text>{props.text}</Text>
                <Btn
                    buttonBg={props.buttonBg}
                    click={() => props.func()}
                    text={props.action[0]}
                    default={'default'}> </Btn>
                <Btn
                    buttonBg={props.buttonBg}
                    click={() => props.closeWindow()}
                    text={props.action[1]}
                    default={'default'}> </Btn>
            </ModalWindow>
        </ModalBg>
    )
};

Modal.defaultProps = {
    header: ['Modal window', 'Modal window'],
    action: ['OK', 'Cancel'],
    buttonBg: '#00000015'
};

export default Modal
Modal.propsTypes = {
    func: PropTypes.func,
    header: PropTypes.string,
    action: PropTypes.array
};