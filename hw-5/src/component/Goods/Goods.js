import React, {useState} from 'react';
import {CardBox, Img, Text, Star} from './GoodsStyle.js'
import Button from '../Button/Button.js'
import Modal from "../Modal/Modal";
import {openWindow, closeWindow} from "../../reducers/modal/action";
import {cartSet, favoriteSet} from "../../reducers/item/services";
import {connect} from "react-redux";

const Goods = (props) => {
    const {
        bg,
        name,
        color,
        price,
        id,
        favorite,
        cart,
        cartSet,
        openWindow,
        closeWindow,
        modalState,
        favoriteSet
    } = props, [star, toggleStar] = useState(!!favorite.find(elem => elem.id === id));

    let includeInCart = !!cart.find(elem => elem.id === id), changeValue = {
        text: (includeInCart && 'Remove') || 'Add To Cart',
        header: (includeInCart && 'Remove From Cart') || 'Add To Cart',
        modalWindowText: (includeInCart &&
            'Are you sure you want to remove this item from your cart?') ||
            'Are you sure you want to add this product to your cart',
    };

    const Product = {
        "product": name,
        "price": price,
        "image": bg,
        "color": color,
        "id": id
    }, openModal = () => {
        openWindow(id)
    }, cartSetFunc = () => {
        closeWindow();
        cartSet(cart, Product);
    }, favoriteSetFunc = () => {
        favoriteSet(favorite, Product);
        toggleStar(!!favorite.find(elem => elem.id === id));
    };

    return (
        <>
            {modalState === id &&
            <Modal
                closeWindow={closeWindow}
                func={cartSetFunc}
                header={changeValue.header}
                text={changeValue.modalWindowText}
                action={['OK', 'Cancel']}> </Modal>}
            <CardBox>
                <Img
                    bg={`./assets/${bg}`}><Star
                    onClick={favoriteSetFunc}
                    active={star}>
                </Star></Img>
                <Text
                    bold>{name}</Text>
                <Text>Color: {color}</Text>
                <Text
                    bold>Price: {price}
                </Text>
                <Button
                    buttonBg={changeValue.buttonBg}
                    text={changeValue.text}
                    click={openModal}/>
            </CardBox>
        </>
    );

}, mapStateToProps = ({item, window}) => ({
    favorite: item.favorite,
    cart: item.cart,
    modalState: window.open,
}), mapDispatchToProps = (dispatch) => ({
    cartSet: (state, id) => dispatch(cartSet(state, id)),
    favoriteSet: (state, id) => dispatch(favoriteSet(state, id)),
    openWindow: (id) => dispatch(openWindow(id)),
    closeWindow: () => dispatch(closeWindow())
});

export default connect(mapStateToProps, mapDispatchToProps)(Goods);

