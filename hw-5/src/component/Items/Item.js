import React, {useState} from 'react';
import FormikForm from "../Cart/Form/Form";
import Goods from "../Goods/Goods";
import BuyButton from '../Cart/Button/ButtonStyle';
import {Grids} from "../../mainStyle";
import {connect} from "react-redux";

const Item = (props) => {
    const [isOpen, openForm] = useState(false), userData = !!props.cart.length;

    return (
        <>
            {props.showCart && userData &&
            <BuyButton
                onClick={() => openForm(true)}>Buy
            </BuyButton>}
            {isOpen && userData &&
            <FormikForm
                inputForm={openForm}
                cart={props.cart}
                sold={props.sold}/>}
            <Grids>
                {props.filter.map(elem => (
                    <Goods
                        key={elem.id}
                        bg={elem.image}
                        name={elem.product}
                        id={elem.id}
                        color={elem.color}
                        price={elem.price}/>
                ))}
            </Grids>
        </>
    )

}, mapStateToProps = ({item}) => ({
    cart: item.cart,
}), mapDispatchToProps = (dispatch) => ({
    sold: () => dispatch({type: "SOLD"})
});

export default connect(mapStateToProps, mapDispatchToProps) (Item);