import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Item, Img, Text, Star} from './GoodsStyle.js'
import Btn from '../Button/Button.js'

export default class Goods extends PureComponent {
    state = {
        star: false
    };

    componentDidMount() {
        const inStorage = localStorage.getItem(`${this.props.id}-star`) === 'true';
        this.setState({star: inStorage});
    }

    starClick() {
        switch (this.state.star) {
            case false:
                localStorage.setItem(`${this.props.id}-star`, 'true');
                this.setState({
                    star: true
                });
                break;
            default:
                localStorage.removeItem(`${this.props.id}-star`);
                this.setState({
                    star: false
                });
                break;
        }
    }

    render() {
        return (
            <Item>
                <Img bg={this.props.bg}>
                    <Star onClick={this.starClick.bind(this)}
                          active={this.state.star}>
                    </Star>
                </Img>
                <Text bold>{this.props.name}</Text>
                <Text>Color : {this.props.color}</Text>
                <Text bold>Price : {this.props.price}</Text>
                <Btn buttonBg={this.props.buttonBg}
                     text={['Add To Cart', 'Remove']}
                     header={['Add To Cart', 'Remove From Cart']}
                     id={this.props.id}
                     modalWindowText={['Are you sure you want to add this product to your cart',
                         'Are you sure you want to remove this item from your cart?']}/>
            </Item>
        );
    }
}

Goods.propTypes = {
    buttonBg: PropTypes.string,
    bg: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};
