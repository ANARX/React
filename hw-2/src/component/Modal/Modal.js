import React, {PureComponent} from 'react';
import {ModalBg, ModalWindow, Header, Text} from './ModalStyles.js'
import Btn from '../Button/BtnStyle.js'
import PropTypes from "prop-types";

export default class Modal extends PureComponent {
    outsideClick(e) {
        if (e.target === e.currentTarget) {
            this.props.func()
        }
    };

    render() {
        return (
            <ModalBg onClick={(e) => this.outsideClick(e)}>
                <ModalWindow>
                    <Header>
                        {this.props.header}
                    </Header>
                    <Text>{this.props.text}</Text>
                    <Btn buttonBg={this.props.buttonBg} default
                                   onClick={() => this.props.func(true)}>{this.props.action[0]}</Btn>
                    <Btn buttonBg={this.props.buttonBg} default
                                   onClick={() => this.props.func()}>{this.props.action[1]}</Btn>
                </ModalWindow>
            </ModalBg>
        );
    }
}

Modal.propsTypes = {
    func: PropTypes.func,
    action: PropTypes.array
};

Modal.defaultProps = {
    header: ['Modal Window', 'Modal Window'],
    action: ['OK', 'Cancel'],
    buttonBg: '#B3382C'
};