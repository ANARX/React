import styled from "styled-components";

const Btn = styled.button`
        border-radius: 3px;
        font-size: 12pt;
        outline: none;
        border: none;
        cursor: pointer;
        color: #FFFFFF;
        text-transform: uppercase;
        margin: 0px 20px 10px 20px;
        width: ${(props) => (props.default ? '100px' : 'auto')};
        padding: ${(props) => (props.default ? '10px 15px' : '5px 10px')};  
        background: ${(props) => (props.buttonBg)};
        `;
export default Btn;