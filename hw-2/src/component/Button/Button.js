import React, {PureComponent} from 'react';
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import Btn from "./BtnStyle.js";

export default class Button extends PureComponent {
    state = {
        isOpen: false,
        isActive: true,
    };

    componentDidMount() {
        const inStorage = localStorage.getItem(this.props.id) === 'false';
        this.setState({isActive: !inStorage});
    }

    clickFunc(value) {
        switch (value) {
            case true:
                value = !this.state.isActive;
                if (!!localStorage.getItem(this.props.id)) {
                    localStorage.removeItem(this.props.id)
                } else {
                    localStorage.setItem(this.props.id, 'false')
                }
                break;
            default:
                value = this.state.isActive
                break;
        }
        this.setState({
            isOpen: !this.state.isOpen,
            isActive: value
        });
    }

    render() {
        const title = (this.state.isActive && this.props.text[0]) ||
            (!this.state.isActive && this.props.text[1]);
        const header = (this.state.isActive && this.props.header[0]) ||
            (!this.state.isActive && this.props.header[1]);
        const modalText = (this.state.isActive && this.props.modalWindowText[0]) ||
            (!this.state.isActive && this.props.modalWindowText[1]);
        return (
            <>
                <Btn buttonBg={this.props.buttonBg}

                     onClick={() => {
                         this.clickFunc('true')
                     }}>
                    {title}
                </Btn>
                {this.state.isOpen &&
                <Modal action={this.props.action}
                       header={header}
                       func={this.clickFunc.bind(this)}
                       text={modalText}> </Modal>}
            </>

        );
    }
}
Button.propTypes = {
    text: PropTypes.array,
    header: PropTypes.array,
    modalWindowText: PropTypes.array,
    action: PropTypes.array,
    price: PropTypes.string,
    buttonBg: PropTypes.string,
    position: PropTypes.string,
    margin: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};
Button.defaultProps = {
    position: 'center',
    buttonBg: '#8E9CAC',
};