import React, {Component} from 'react';
import axios from 'axios'
import MainStyle from "./mainStyle";
import Goods from "./component/Goods/Goods";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount() {
        axios("/db.json")
            .then(res => this.setState({items: res.data}))
    }

    render() {
        return (
            <MainStyle>
                {this.state.items.map(elem => (
                    <Goods key={elem.id}
                           bg={elem.image}
                           name={elem.product}
                           id={elem.id}
                           color={elem.color}
                           price={elem.price}/>
                ))}
            </MainStyle>
        )
    }
}

export default App;
