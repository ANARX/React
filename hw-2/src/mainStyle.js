import styled from "styled-components";

const MainStyle = styled.div`
        background-image: url('/assets/bg.jpg');
        background-repeat: no-repeat;
        background-size: 100%;
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        grid-gap: 35px;
        padding: 10px;
        `;
export default  MainStyle