import React, {useState, useEffect} from 'react';
import Main from './component/Nav/Nav'
import axios from 'axios'
import {Route} from "react-router-dom";
import Item from "./component/Items/Item";
import {Root, Grids} from "./mainStyle";

const App = () => {
    const [items, addItems] = useState([]);
    const [cart, reworkCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [star, reworkStar] = useState(JSON.parse(localStorage.getItem('star')) || []);
    let cartInfo = {
        info: cart,
        func: reworkCart
    };
    let starInfo = {
        info: star,
        func: reworkStar
    };

    useEffect(() => {
        if (items.length === 0) {
            axios("/db.json")
                .then(res => addItems(res.data))
        }
    }, [cartInfo, items.length, starInfo]);

    return (
        <>
            <Root>
                <Main/>
                <Grids>
                    <Route exact path='/'
                           render={() => <Item
                               items={items}
                               cart={cartInfo}
                               star={starInfo}/>}/>
                    <Route exact path='/cart'
                           render={() => <Item
                               items={items}
                               filter={'cart'}
                               cart={cartInfo}
                               star={starInfo}/>}/>
                    <Route exact path='/favorite'
                           render={() => <Item
                               items={items}
                               filter={'star'}
                               cart={cartInfo}
                               star={starInfo}/>}/>
                </Grids>
            </Root>
        </>
    )
};

export default App;