import styled from "styled-components";

const ModalBg = styled.div`
        position: fixed;
        z-index: 3;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        background: #00000050;
        text-align: center;
        `;

const ModalWindow = styled.div`
        background: ${(props) => props.bg};
        margin: 0;
        width: 35%;
        color: #FFFFFF;
        `;

const Header = styled.h1`
         margin: 0;
         text-align: left;
         background: #D44637;
         padding: 10px 20px
        `;

const Text = styled.p`
         padding: 0 55px;
         font-size: 14pt;
         line-height: 40px;
`;

export {
    ModalBg,
    ModalWindow,
    Header,
    Text
}