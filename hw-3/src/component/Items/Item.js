import React from 'react';
import Goods from "../Goods/Goods";

const Item = (props) => {
    let {items, filter} = props;
    if (filter) {
        items = items
            .filter(elem => JSON.parse((localStorage.getItem(filter) || [])
                .includes(elem.id)));
    }
    return (
        items
            .map(elem => (
                <Goods key={elem.id}
                       bg={elem.image}
                       buttonBg={'#00000010'}
                       name={elem.product}
                       id={elem.id}
                       cart={props.cart}
                       star={props.star}
                       color={elem.color}
                       price={elem.price}/>
            ))

    )
};
export default Item;