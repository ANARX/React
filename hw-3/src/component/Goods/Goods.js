import React, {useState} from 'react';
import {CardBox, Img, Text, Star} from './GoodsStyle.js'
import Button from '../Button/Button.js'
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";


const Goods = (props) => {
    const {func, info: inStorage} = props.star, cartProps = props.cart, [isOpen, open] = useState(false),
        favorite = () => {
            if (!inStorage.includes(props.id)) {
                inStorage.push(props.id);
                localStorage.setItem(`star`, JSON.stringify(inStorage));
            } else {
                inStorage.splice(inStorage.indexOf(props.id), 1);
                localStorage.setItem(`star`, JSON.stringify(inStorage));
            }
            func(JSON.parse(localStorage.getItem('star')));
        }, cart = () => {
            open(!isOpen);
        }, selectItem = (value) => {
            if (value) {
                const inStorageCart = cartProps.info;
                if (!inStorageCart.includes(props.id)) {
                    inStorageCart.push(props.id);
                    localStorage.setItem(`cart`, JSON.stringify(inStorageCart));
                } else {
                    inStorageCart.splice(inStorageCart.indexOf(props.id), 1);
                    localStorage.setItem(`cart`, JSON.stringify(inStorageCart));
                }
                cartProps.func(JSON.parse(localStorage.getItem('cart')));
            }
            open(!isOpen);
        }, changeValue = {
            text: (cartProps.info.includes(props.id) && 'Remove') || 'Add To Cart',
            header: (cartProps.info.includes(props.id) && 'Remove From Cart') || 'Add To Cart',
            modalWindowText: (cartProps.info.includes(props.id) &&
                'Are you sure you want to remove this item from your cart?') ||
                'Are you sure you want to add this product to your cart'
        };
    return (
        <>
            {isOpen && <Modal
                margin={0}
                func={selectItem}
                header={changeValue.header}
                text={changeValue.modalWindowText}
                action={['OK', 'Cancel']}
                background={props.buttonBg}> </Modal>}
            <CardBox>
                <Img
                    bg={`./assets/${props.bg}`}><Star
                    onClick={favorite}
                    active={inStorage.includes(props.id)}>
                </Star></Img>
                <Text
                    bold>{props.name}</Text>
                <Text>Color: {props.color}</Text>
                <Text
                    bold>Price: {props.price}</Text>
                <Button
                    buttonBg={props.buttonBg}
                    text={changeValue.text}
                    click={cart}/>
            </CardBox>
        </>
    )
};

Goods.propTypes = {
    buttonBg: PropTypes.string,
    bg: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};

export default Goods;