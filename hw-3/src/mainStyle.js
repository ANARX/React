import styled from "styled-components";

const Root = styled.div`
        background-image: url('assets/bg.jpg');
        background-repeat: no-repeat;
        background-size: 100%;
        min-height: 100vh;
`;

const Grids = styled.div`
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        grid-gap: 25px;
        padding: 20px;
`;

export {
    Root,
    Grids
}