import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Route} from "react-router-dom";
import Item from "./component/Items/Item";
import {Root, Grids} from "./mainStyle";
import Main from './component/Nav/Nav';
import {getData} from './reducers/data/service';
import {cartSet, favoriteSet} from "./reducers/item/service";

const App = (props) => {
    const {data, loadedData, cartSet, favoriteSet, cart, favorite} = props;
    useEffect(() => {
        if (!data.length) {
            loadedData()
        }
        favoriteSet(JSON.parse(localStorage.getItem('favorite')) || []);
        cartSet(JSON.parse(localStorage.getItem('cart')) || [])
    }, []);

    const itemFilter = i => data.filter(e => i.includes(e.id));

    return (
        <>
            {data.length && <Root>
                <Main/>
                <Grids>
                    <Route exact path='/'
                           render={() =>
                               <Item
                                   filter={data}/>}/>
                    <Route exact path='/cart'
                           render={() =>
                               <Item
                                   filter={itemFilter(cart)}/>}/>
                    <Route exact path='/favorite'
                           render={() =>
                               <Item
                                   filter={itemFilter(favorite)}/>}/>
                </Grids>
            </Root>}
        </>
    )

}, mapStateToProps = ({loadData, item}) => ({
    data: loadData.data,
    isLoading: loadData.isLoading,
    favorite: item.favorite,
    cart: item.cart,
}), mapDispatchToProps = (dispatch) => ({
    loadedData: () => dispatch(getData()),
    cartSet: (state, id) => dispatch(cartSet(state, id)),
    favoriteSet: (state, id) => dispatch(favoriteSet(state, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
