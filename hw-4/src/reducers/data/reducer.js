const initialState = {
    data: [],
    isLoading: false
}, reducer = (state = initialState, action) => {
    switch (action.type) {
        case "LOAD_DATA":
            return {...state, isLoading: action.payload};
        case "SAVE_DATA":
            return {...state, data: action.payload};
        default:
            return state;
    }
};

export default reducer;