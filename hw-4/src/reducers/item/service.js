import {toFavorite, toCart, removeFavorite, removeCart} from './actions';

export const cartSet = (state, id) => (dispatch) => {
    if (!id) {
        dispatch({
            type: 'TO_CART',
            payload: state
        })
    } else if (!state.includes(id)) {
        dispatch(toCart(state, id))
    } else {
        dispatch(removeCart(state, id));
    }
}, favoriteSet = (state, id) => (dispatch) => {
    if (!id) {
        dispatch({
            type: 'TO_FAVORITE',
            payload: state
        })
    } else if (!state.includes(id)) {
        dispatch(toFavorite(state, id))
    } else {
        dispatch(removeFavorite(state, id));
    }
};