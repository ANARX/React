const cutFunc = (array, elem) => {
    array.splice(array.indexOf(elem), 1);
    return array
}, stateFunc = (array, elem) => {
    array.push(elem);
    return array
};

export const toFavorite = (array, elem) => ({
    type: 'TO_FAVORITE',
    payload: stateFunc(array, elem)
}), removeFavorite = (array, elem) => ({
    type: 'REMOVE_FAVORITE',
    payload: cutFunc(array, elem)
}), toCart = (array, elem) => ({
    type: 'TO_CART',
    payload: stateFunc(array, elem)
}), removeCart = (array, elem) => ({
    type: 'REMOVE_CART',
    payload: cutFunc(array, elem)
});