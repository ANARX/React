import React from 'react';
import Goods from "../Goods/Goods";
import {connect} from "react-redux";

const Item = (props) => {
    return (
        props.filter.map(elem => (
            <Goods
                key={elem.id}
                bg={elem.image}
                name={elem.product}
                id={elem.id}
                color={elem.color}
                price={elem.price}/>
        ))
    )
}, mapStateToProps = ({item}) => ({
    favorite: item.favorite,
    cart: item.cart,
});

export default connect(mapStateToProps) (Item);