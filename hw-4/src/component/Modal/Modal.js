import React from 'react';
import {ModalBg, ModalWindow, Header, Text} from './ModalStyle.js';
import Btn from "../Button/Button";
import PropTypes from "prop-types";

const Modal = (props) => {
    const outsideClick = (event) => {
        if (event.target === event.currentTarget) {
            props.closeWindow()
        }
    };
    return (
        <ModalBg onClick={(event) => outsideClick(event)}>
            <ModalWindow bg={'#E74C3C'}>
                <Header>
                    {props.header}
                </Header>
                <Text>{props.text}</Text>
                <Btn
                    buttonBg={props.buttonBg}
                    click={() => props.func()}
                    text={props.action[0]}
                    default={'default'}>
                </Btn>
                <Btn
                    buttonBg={props.buttonBg}
                    click={() => props.closeWindow()}
                    text={props.action[1]}
                    default={'default'}>
                </Btn>
            </ModalWindow>
        </ModalBg>
    )
};
export default Modal
Modal.propsTypes = {
    func: PropTypes.func,
    header: PropTypes.string,
    action: PropTypes.array
};
Modal.defaultProps = {
    header: ['Modal Window', 'Modal Window'],
    action: ['OK', 'Cancel'],
    buttonBg: '#00000015'
};