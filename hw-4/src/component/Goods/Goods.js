import React, {useState} from 'react';
import {CardBox, Img, Text, Star} from './GoodsStyle.js';
import Button from '../Button/Button.js';
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
import {connect} from "react-redux";
import {openWindow, closeWindow} from "../../reducers/modal/actions";
import {cartSet, favoriteSet} from "../../reducers/item/service";

const Goods = (props) => {
    const [star, toggleStar] = useState(props.favorite.includes(props.id));

    let changeValue = {
        text: (props.cart.includes(props.id) && 'Remove') || 'Add To Cart',
        header: (props.cart.includes(props.id) && 'Remove From Cart') || 'Add To Cart',
        modalWindowText: (props.cart.includes(props.id) &&
            'Are you sure you want to remove this item from your cart?') ||
            'Are you sure you want to add this product to your cart',
    };

    const openModal = () => {
        props.openWindow(props.id)
    }, cartSetFunc = () => {
        props.closeWindow();
        props.cartSet(props.cart, props.id);
        localStorage.setItem('cart', JSON.stringify(props.cart));
    }, favoriteSetFunc = () => {
        props.favoriteSet(props.favorite, props.id);
        toggleStar(props.favorite.includes(props.id));
        localStorage.setItem('favorite', JSON.stringify(props.favorite));
    };

    return (
        <>
            {props.modalState === props.id &&
            <Modal
                closeWindow={props.closeWindow}
                func={cartSetFunc}
                header={changeValue.header}
                text={changeValue.modalWindowText}
                action={['OK', 'Cancel']}> </Modal>}
            <CardBox>
                <Img
                    bg={`./assets/${(props.bg)}`}><Star
                    onClick={favoriteSetFunc}
                    active={star}>
                </Star></Img>
                <Text
                    bold>{props.name}</Text>
                <Text>Color: {props.color}</Text>
                <Text
                    bold>Price : {props.price}
                </Text>
                <Button
                    buttonBg={changeValue.buttonBg}
                    text={changeValue.text}
                    click={openModal}/>
            </CardBox>
        </>
    );

}, mapStateToProps = ({item, window}) => ({
    favorite: item.favorite,
    cart: item.cart,
    modalState: window.open,
}), mapDispatchToProps = (dispatch) => ({
    cartSet: (state, id) => dispatch(cartSet(state, id)),
    favoriteSet: (state, id) => dispatch(favoriteSet(state, id)),
    openWindow: (id) => dispatch(openWindow(id)),
    closeWindow: () => dispatch(closeWindow())
});

Goods.propTypes = {
    buttonBg: PropTypes.string,
    bg: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(Goods);