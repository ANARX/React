import React from 'react';
import Btn from './BtnStyle.js';

const Button = (props) => {
    return (
        <>
            <Btn
                buttonBg={props.buttonBg}
                position={props.position}
                margin={props.margin}
                default={props.default}
                onClick={() => {
                    props.click()
                }}>
                {props.text}
            </Btn>
        </>
    )
};

export default Button
