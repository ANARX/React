import React from "react";
import {Header, Store} from "./NavStyle";
import {Link} from "react-router-dom";

const Main = () => {
    return (
        <Header>
            <Link to={"/"}><Store>Main</Store></Link>
            <Link to={"/cart"}><Store>Cart</Store></Link>
            <Link to={"/favorite"}><Store>Favorite</Store></Link>
        </Header>
    )
};

export default Main;