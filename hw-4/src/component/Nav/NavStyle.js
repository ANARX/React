import styled from "styled-components";

const Header = styled.ul`
       display: inline-flex;
       font-size: 25pt;
       background: #31415850;
       box-shadow: 5px 5px 15px 3px rgba(0, 0, 0, 0.5);
`;
const Store = styled.li`
       padding: 20px; 
       border-left: 5px solid #2D3D54; 
       z-index: 2;
       transition: all 0.7s ease 0s; 
       text-decoration: none;
       color: #00000090;
        &:hover{
        border-left: 5px solid #bdcdf0; 
        box-shadow: 3px 3px 7px 5px rgba(0, 0, 0, 0.5);
        color: #bdcdf0;
        }
`;

export {
    Header,
    Store
}